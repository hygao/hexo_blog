---
title: 一个有关cantor函数的讨论
date: 2023-12-4
tags: math
mathjax: true
---
### 一个有关cantor函数的讨论

首先$\forall x\in]0,1],k\in N^*,x$能唯一表示为$k$进制的形式
$\sum_{n=1}^{\infty}\frac{x_n}{k^n},x_n\in[0,k-1]\cap N,lim_{n\rightarrow \infty} x_n \neq 0$,
不妨将其记为$x = \{x_n\}_k = \sum_{n=1}^{\infty}\frac{x_n}{k^n}$

则$\forall \{x_n\}_k,\{y_n\}_k\in[0,1]$,
$\{x_n\}_k<\{y_n\}_k \iff \exists m\in N^*,\forall i<m,x_i=y_i$但$x_m<y_m$

下定义$f:[0,1]\rightarrow[0,1]$
$f(x) = \left\{\begin{matrix} \{2x_n\}_3,x=\{x_n\}_2\\ 0,x=0\end{matrix}\right.$

定理1：
$f$满足以下性质：
* $f$在$[0,1]$上严格递增
* $f$在$]0,1]$左连续,在$x=0$处右连续
* $f$在$[0,1]-Q$上连续,在$]0,1[\cap Q$上不连续

证明：
* $\forall x,y \in ]0,1],x<y,$
  $f(x) = f(\{x_n\}_2) = \{2x_n\}_3 < \{2y_n\}_3 = f(\{y_n\}_2) = f(y)$
* $\forall x\in]0,1], x = \{x_n\}_2.$
  由于$lim_{n\rightarrow \infty} x_n \neq 0,\forall n\in N^*,\exists m>n,x_m = 1.$
  则$f(x-\frac{1}{2^m}) = f(\{x_n\}_2-\frac{1}{2^m}) = \{2x_n\}_3-\frac{2}{3^m}$
  对$n$取极限$,f(x-0) = \{2x_n\}_3 = f(x).$
  $\forall n>0,f(\frac{1}{2^n}) = \frac{2}{3^n}.$
  对$n$取极限有$f(0+0) = 0$
* $\forall x\in]0,1]-Q, x = \{x_n\}_2,\forall n\in N^*,\exists m>n,x_m = 0.$
  则$f(x+\frac{1}{2^m}) = f(\{x_n\}_2+\frac{1}{2^m}) = \{2x_n\}_3+\frac{2}{3^m}.$
  对$n$取极限$,f(x+0) = \{2x_n\}_3 = f(x).$
  即$f(x)$在$[0,1]-Q$上连续.


  $\forall x\in]0,1[\cap Q, x = \{x_n\}_2,\exists n'>0,x_{n'} = 0,\forall i>n',x_i = 1.$
  $\forall m>n',x+\frac{1}{2^m} = \{x'_n\}_2$
  其中$x'_n = \left\{\begin{matrix} x_n,n<m\\ 1,x\in\{n',m\} \\ 0,else\end{matrix}\right.$
  则$f(x+\frac{1}{2^m}) = \{2x'_n\}_3 = \{2x_n\}_3+\frac{1}{3^{n'}}+\frac{2}{3^m}.$
  对$m$取极限$,f(x+0) = \{2x_n\}_3+\frac{1}{3^{n'}} = f(x)+\frac{1}{3^{n'}}.$
  即$f(x)$在$]0,1[\cap Q$上不连续.

$\square$

容易看出$ran(f) = \{\{t_n\}_3 \mid t_n\in\{0,2\}\}\subsetneqq [0,1]$,
下定义函数$F:[0,1]\rightarrow ran(f)$
$F(x) = f(x)$
容易验证$F$是双射
$F^{-1}(x) = \left\{\begin{matrix} \{\frac{x_n}{2}\}_2,x=\{x_n\}_3\\ 0,x=0\end{matrix}\right.$

记$H:[0,1]\rightarrow ran(f)$
$H(x) = \left\{\begin{matrix} \{h(x_n)\}_3,x=\{x_n\}_3\\ 0,x=0\end{matrix}\right.$
若$1\notin \{x_n\},h(x_n) = x_n$
否则$\exists k:=l(x_n)>0,\forall i<k,a_i\neq 1,a_k = 1$
$h(x_n) = \left\{\begin{matrix} x_n,n<k\\ 0,x=k\\2,n\ge k\end{matrix}\right.$

有以下定理
定理2：
$f^{(-1)}(y) = F^{-1}\circ H(x) = \left\{\begin{matrix} \{\frac{h(x_n)}{2}\}_2,x=\{x_n\}_3\\ 0,x=0\end{matrix}\right.$

证明：
显然$f^{(-1)}(0) = 0,f^{(-1)}(1) = 1$
$\forall y\in]0,1[,f^{(-1)}(y) = \sup\{x\in[0,1]\mid f(x)<y\}$
$=\sup\{F^{-1}(t)\mid t<y,t\in ran(f)\}$
$=F^{-1}(\sup\{t\in ran(f)\mid t<y\})$
下证$\sup\{t\in ran(f)\mid t<y\} = H(y)$
设$y = \{y_n\}_3,H(y) = \{h(x_n)\}_3\le \{y_n\}_3 = y$且$H(y)\in ran(f)$
$\forall t\in ran(f),t = \{t_n\}_3>H(y) \iff \exists k>0,\forall i<k,t_i = \\ h(x_i),t_k>h(x_k)$
若$1\notin \{x_n\},t>\{x_n\}_3 = x.$
否则若$k<l(x_n),t_k>h(x_k) = x_k\Rightarrow t>x$
若$k=l(x_n),t_k>h(x_k) = 0,t_k=2>1 = x_k\Rightarrow t>x$
若$k>l(x_n),t_k>h(x_k) = 2$矛盾.
综上$,t>x$
即$\sup\{t\in ran(f)\mid t<y\} = H(y)$
$f^{(-1)}(y) = F^{-1}\circ H(x)$

$\square$

由于$f$在$[0,1]$左连续以及严格单调，可以得到以下结论
推论1：
* $(f^{(-1)})^{(-1)} = f.$
* $f^{(-1)}\circ f= id_{[0,1]}.$
* $f$在$[0,1]$上连续

$f^{(-1)}(y)$就是大名鼎鼎的cantor函数

